import {Logger} from "./logger";
import {SimpleTestLauncher} from "./simple-test-launcher";

export namespace testLauncher {

  /**
   * Contains the configuration
   */
  export namespace configuration {

    export namespace coverage {

      export interface Summary {
        enabled?: boolean;
        template: string;
      }

      export interface Configuration {
        enabled?: boolean;
        summary?: Summary;
      }
    }

    /**
     * Configuration about the reports
     */
    export namespace reports {
      export interface Configuration {
        /**
         * The directory path for all reports. If not project auto generated
         */
        rootDirectoryPath: string;
      }
    }

    /**
     * Configuration about the test
     */
    export namespace test {

      export interface WithJasmineFeatures {
        /**
         * true to enable the report as junit xml
         */
        enableJunitXml?: boolean;
        /**
         * true to enable the report as HTML
         */
        enableHtml?: boolean;
      }

      export interface TestWithJasmine {
        /**
         * The jasmine feature
         */
        features?: WithJasmineFeatures;
        /**
         * The native configuration, will override the one generated
         */
        nativeConfig: object | undefined;
      }

      /**
       * Spec filter
       */
      export type SpecFileFilter = (filePath: string) => Promise<boolean|undefined|null>;

      /**
       * The test configuration
       */
      export interface Configuration {
        /**
         * The test type
         */
        type: string;
        /**
         * The spec directory path
         */
        specDirPath?: string | undefined;
        /**
         * Define some filter to apply to each file. When using regexp, if match, the file is ignored
         */
        specFileFilters?: Array<string|SpecFileFilter|RegExp>;
        /**
         * The jasmine configuration part
         */
        jasmine?: TestWithJasmine;
      }
    }

    /**
     * Configuration about the project
     */
    export interface Project {
      /**
       * The root directory path of your project
       */
      rootDirectoryPath: string;
    }

    export interface Configuration {
      /**
       * The project root directory. This is important for the coverage
       */
      rootDirectory: string;

      /**
       * The project configuration
       */
      project: Project;

      /**
       * The test configuration
       */
      test: test.Configuration;

      /**
       * The reports
       */
      reports: reports.Configuration;

      /**
       * The coverage
       */
      coverage: coverage.Configuration;

      /**
       * The steps configuration
       */
      steps: Steps;
    }
  }

  export interface Steps {
    /**
     * The pre launch steps (before anything)
     */
    preLaunch: Step[];
    /**
     * The step executed just before the test but after the the coverage
     */
    preTestLaunch: Step[];
    /**
     * The step just after the execution finished
     */
    postExecution: Step[];
    /**
     * The clean up steps. (Always executed)
     */
    cleanUp: Step[];
  }

  export interface BaseStepHandlerContext {
    launcher: SimpleTestLauncher;
    logger: Logger;
    executionContext: object;
  }

  export interface PostExecutionStepHandlerContext extends BaseStepHandlerContext {
    testSucceed: boolean;
  }

  export interface StepHandlerOnResolve {
    stopWorkflow?: boolean | null | undefined;
  }

  export type StepHandler = (context: BaseStepHandlerContext) => Promise<StepHandlerOnResolve | null | undefined>;

  export interface Step {
    /**
     * The function to be executed
     */
    handler: StepHandler;
  }
}

export interface TestLauncher {

  /**
   * Initialize the launcher
   * @param options The options
   */
  initialize(options: testLauncher.configuration.Configuration): Promise<void>;

  /**
   * Execute the launcher
   */
  run(): Promise<void>;

  /**
   * Check if the launcher is ready
   */
  isReady(): boolean;

  /**
   * Check if the test is running
   */
  isRunning(): boolean;

  /**
   * Shut down the test. This will exit the process
   */
  shutDown();

  /**
   * Merge multiple configuration to 1
   * @param args Configurations
   */
  mergeConfiguration(...args: testLauncher.configuration.Configuration[]): testLauncher.configuration.Configuration;
}


import {createLogger, Logger} from "./logger";
import * as _ from 'lodash';
import {EventEmitter} from "events";
import {jasmineRunner, JasmineRunner} from './jasmine-runner';

function lodashMergerWithIgnoreNil(objValue, srcValue) {
  if (_.isNil(srcValue)) {
    return objValue;
  }
}

export namespace simpleJasmineRunner {

  export interface LastExecution {
    success: boolean;
  }

  export interface Properties {
    instance: object | undefined;
    lastExecution: LastExecution;
    specFileFilters?: Array<string|jasmineRunner.TestSpecFileFilter|RegExp>;
    projectDirectoryPath?: string;
    specDirPath?: string;
    reportDirectoryPath?: string;
    features?: jasmineRunner.ConfigurationFeatures;
  }

  export interface FilterTestSpecFilesOptions {
    filePaths: string[];
  }

  export interface FilterTestSpecFilesOnResolve {
    filePaths: string[];
  }

  export interface GenerateConfigurationOnResolve {
    configuration: object;
  }

  export interface CreateReportersOnResolve {
    reporters: object[];
  }
}



/**
 * Jasmine runner
 */
export class SimpleJasmineRunner extends EventEmitter implements JasmineRunner {

  protected logger: Logger;

  protected properties: simpleJasmineRunner.Properties;

  constructor() {
    super();

    this.logger = createLogger('jasmine');

    this.properties = {
      instance: undefined,
      lastExecution: {
        success: false
      },
      specFileFilters: []
    };
  }

  public get lastExecutionSucceed(): boolean {
    return this.properties.lastExecution.success;
  }

  /**
   * Initialize the runner
   * @param configuration The runner configuration
   */
  public async initialize(configuration: jasmineRunner.Configuration): Promise<void> {
    this.properties.projectDirectoryPath = configuration.projectDirectoryPath;
    this.properties.specFileFilters = configuration.specFileFilters;
    this.properties.specDirPath = configuration.specDirPath;
    this.properties.features = configuration.features;
    this.properties.reportDirectoryPath = configuration.reportDirectoryPath;

    this.logger.debug(`Create and initialize the jasmine instance`);
    await this.createAndInitInstance();
  }

  /**
   * Start the jasmine execution
   */
  public async start(options: jasmineRunner.StartOptions): Promise<void> {
    // @ts-ignore
    this.properties.instance.onComplete(passed => {
      this.properties.lastExecution.success = !!passed;
      setImmediate(() => this.onJasmineFinished());
    });

    // @ts-ignore
    this.properties.instance.env.executionContext = options.executionContext;

    this.logger.info(`Start the execution`);
    // @ts-ignore
    this.properties.instance.execute();
  }

  public run(options: jasmineRunner.StartOptions): Promise<jasmineRunner.events.Finished> {
    return new Promise<jasmineRunner.events.Finished>((resolve, reject) => {
      this.once('finished', resolve);

      this.start(options)
        .catch(reject);
    });
  }

  /**
   * Generate the jasmine configuration
   */
  protected async generateConfiguration(): Promise<simpleJasmineRunner.GenerateConfigurationOnResolve> {
    const defaultJasmineConfiguration = {
      spec_dir: this.properties.specDirPath,
      spec_files: [
        "**/*[sS]pec.js",
        '*[sS]pec.js',
        "**/*[sS]pec.ts",
        '*[sS]pec.ts'
      ],
      stopSpecOnExpectationFailure: false,
      random: false
    };

    const jasmineConfiguration = _.mergeWith({},
      defaultJasmineConfiguration,
      {
        features: this.properties.features
      },
      lodashMergerWithIgnoreNil
    );

    return {
      configuration: jasmineConfiguration
    };
  }

  /**
   * Filters the spec files with user custom filters
   * @param options Options
   */
  protected async filterTestSpecFiles(options: simpleJasmineRunner.FilterTestSpecFilesOptions): Promise<simpleJasmineRunner.FilterTestSpecFilesOnResolve> {
    if (!this.properties.specFileFilters || this.properties.specFileFilters.length === 0) {
      return {
        filePaths: options.filePaths
      };
    }

    // Generate the filters
    const filters = this.properties.specFileFilters.map(filter => {
      if (_.isString(filter) || _.isRegExp(filter)) {
        let regexpFilter = filter;
        if (_.isString(filter)) {
          // Create a regexp
          regexpFilter = new RegExp(filter);
        }

        return async (filePath: string) => {
          const matches = filePath.match(regexpFilter);
          const ignoreFile = _.isArray(matches) && matches.length > 0;
          return {
            ignore: ignoreFile
          };
        };
      } else {
        return filter;
      }
    }) as jasmineRunner.TestSpecFileFilter[];

    const filePaths = [];

    for (const filePath of options.filePaths) {
      let keepFile = true;

      for (const filter of filters) {
        const result = await filter(filePath);

        if (result && result.ignore === true) {
          keepFile = false;
        }
      }

      if (keepFile) {
        filePaths.push(filePath);
      }
    }

    return {
      filePaths
    };
  }

  /**
   * Create the jasmine reports
   */
  protected async createReporters(): Promise<simpleJasmineRunner.CreateReportersOnResolve> {
    const reporters = [];

    if (_.get(this.properties, 'features.reports.enableJunitXml')) {
      const jasmineReporters = require('jasmine-reporters'),
        reporter = new jasmineReporters.JUnitXmlReporter({
          savePath: `${this.properties.reportDirectoryPath}/junit/xml`,
          consolidateAll: false
        });

      reporters.push(reporter);
    }

    if (_.get(this.properties, 'features.reports.enableHtml')) {
      const Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter'),
        reporter = new Jasmine2HtmlReporter({
          savePath: `${this.properties.reportDirectoryPath}/html`,
          screenshotsFolder: 'images',
          takeScreenshots: false
        });

      reporters.push(reporter);
    }

    return {
      reporters
    };
  }

  /**
   * Create and initialize the jasmine instance
   */
  protected async createAndInitInstance(): Promise<void> {
    const Jasmine = require('jasmine');
    const jasmineOptions = {
      projectBaseDir: this.properties.projectDirectoryPath
    };

    const jasmineInstance = new Jasmine(jasmineOptions);

    this.logger.debug(`Generate configuration`);
    const {configuration} = await this.generateConfiguration();
    jasmineInstance.loadConfig(configuration);

    this.logger.debug(`Generate reporters`);
    const {reporters} = await this.createReporters();
    reporters.forEach(reporter => jasmineInstance.addReporter(reporter));

    this.logger.debug(`Filter spec files`);
    const {filePaths} = await this.filterTestSpecFiles({
      filePaths: jasmineInstance.specFiles
    });
    this.logger.debug(`Use those files:\n${filePaths.join('\n')}\n`);
    jasmineInstance.specFiles = filePaths;

    this.properties.instance = jasmineInstance;
  }

  /**
   * Executed when jasmin finished
   */
  protected onJasmineFinished() {
    this.emit('finished', {
      _origin: this,
      success: this.properties.lastExecution.success
    });
  }
}

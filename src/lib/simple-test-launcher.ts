import {EventEmitter} from "events";
import * as _ from 'lodash';
import {classUtils} from './utils/class-utils';
import {createLogger, Logger} from "./logger";
import {SimpleJasmineRunner} from "./simple-jasmine-runner";
import {TestLauncher, testLauncher} from './test-launcher';
import {CoverageRunner} from "./coverage-runner";
import {SimpleCoverageRunner} from "./simple-coverage-runner";
import * as tsNode from "ts-node/dist";

export namespace simpleTestLauncher {

  export interface Properties {
    steps: testLauncher.Steps;
    executionContext: object;
    processExitCode: number;
    activatedStates: string[];
    jasmineRunner?: SimpleJasmineRunner;
    coverageRunner?: CoverageRunner;
    configuration?: testLauncher.configuration.Configuration;
  }

  export interface ExecuteStepOptions {
    groupName: string;
    context: testLauncher.BaseStepHandlerContext;
    stopOnError?: boolean;
  }
}

export class SimpleTestLauncher extends EventEmitter implements TestLauncher {

  /**
   * Execute the test from the given configuration
   * @param config The configuration
   */
  public static async run(config: testLauncher.configuration.Configuration): Promise<void> {
    const instance = new SimpleTestLauncher();

    const {SimpleTestLauncherConfigFromEnvUtils} = await import('./configs/test-launch-config-env');
    const configEnvs = new SimpleTestLauncherConfigFromEnvUtils();

    await instance.initialize(instance.mergeConfiguration(instance.defaultConfiguration(), configEnvs.generateConfiguration(), config));

    return await instance.run();
  }

  /**
   * Execute the test from the given configuration in background and exit when finished.
   * @param config The configuration
   */
  public static execute(config: testLauncher.configuration.Configuration): void {
    const instance = new SimpleTestLauncher();

    import('./configs/test-launch-config-env')
      .then(({SimpleTestLauncherConfigFromEnvUtils}) => {
        const configEnvs = new SimpleTestLauncherConfigFromEnvUtils();
        return instance.initialize(instance.mergeConfiguration(instance.defaultConfiguration(), configEnvs.generateConfiguration(), config));
      })
      .then(() => instance.run())
      .catch(error => {
        process.stderr.write(`Something goes wrong: ${error.message}\n${error.stack}`);
      })
      .then(() => instance.shutDown());
  }

  protected properties: simpleTestLauncher.Properties;
  protected logger: Logger;

  constructor() {
    super();

    this.logger = createLogger('launcher');
    this.properties = {
      executionContext: {},
      activatedStates: [],
      processExitCode: 5,
      steps: {
        cleanUp: [],
        postExecution: [],
        preLaunch: [],
        preTestLaunch: []
      }
    };
  }

  public defaultConfiguration(): testLauncher.configuration.Configuration {
    // Ignore as we need special config to override
    // @ts-ignore
    return {
      coverage: {
        enabled: !process.env.ZE_DISABLE_CODE_COVERAGE
      },
      project: {
        rootDirectoryPath: undefined
      },
      reports: {
        rootDirectoryPath: undefined
      },
      rootDirectory: undefined,
      steps: {
        cleanUp: [],
        postExecution: [],
        preLaunch: [],
        preTestLaunch: []
      }
    };
  }

  /**
   * Merge multiple configuration to 1
   * @param args Configurations
   */
  public mergeConfiguration(...args: testLauncher.configuration.Configuration[]): testLauncher.configuration.Configuration {
    let config = this.defaultConfiguration();

    for (const item of args) {
      config = _.mergeWith(config, item, (objValue, srcValue) => {
        if (_.isNil(srcValue)) {
          return objValue;
        } else if (_.isArray(objValue)) {
          return objValue.concat(srcValue);
        }
      });
    }

    return config;
  }

  public async initialize(options: testLauncher.configuration.Configuration): Promise<void> {
    if (this.isReady()) {
      this.logger.error(`Already initialized`);
      throw new Error("Already initialized");
    }

    this.properties.configuration = options;
    this.properties.steps = options.steps;

    this.logger.debug(`Initialize ts-node`);
    tsNode.register({});

    this.addActiveState('ready');
  }

  /**
   * Launch the test pipeline
   */
  public async run(): Promise<void> {
    if (this.isRunning()) {
      this.logger.info("Already running");
      return;
    } else if (!this.isReady()) {
      this.logger.error("Launcher not ready");
      throw new Error("Launcher not ready");
    }

    this.addActiveState('running');

    this.logger.debug(`Force the exit code to be in error`);
    this.properties.processExitCode = 6;
    process.exitCode = this.properties.processExitCode;

    try {
      await this.runPreTest();
      await this.runTest();
      await this.runPostTest();
    } catch (error) {
      this.logger.error(`Something went wrong: ${error.message}\n${error.stack}`);
      this.properties.processExitCode = 2;
    }

    this.logger.debug(`Clean up`);
    const cleanUpContext = this.createStepBaseContext();
    this.logger.debug(`Execute cleanUp steps`);
    await this.executeSteps({
      groupName: 'cleanUp',
      context: cleanUpContext,
      stopOnError: false
    });

    this.removeActiveState('running');
    process.exitCode = this.properties.processExitCode;
  }

  public isReady(): boolean {
    return this.properties.activatedStates.indexOf('ready') >= 0;
  }

  public isRunning(): boolean {
    return this.properties.activatedStates.indexOf('running') >= 0;
  }

  public shutDown() {
    this.addActiveState('shuttingDown');
    process.exit(this.properties.processExitCode);
  }

  public getTestType(): string {
    return _.get(this.properties, 'configuration.test.type') || 'unknown';
  }

  public getProjectRootDirPath(): string {
    return _.get(this.properties, 'configuration.project.rootDirectoryPath');
  }

  public getRootReportPath(): string {
    return _.get(this.properties, 'configuration.reports.rootDirectoryPath') || (this.getProjectRootDirPath() + '/reports/test');
  }

  public getRootReportTestPath(): string {
    return `${this.getRootReportPath()}/${this.getTestType()}`;
  }

  public getTestSpecDirPath(): string {
    if (this.properties.configuration.test.specDirPath) {
      // User specify the directory to use
      return this.properties.configuration.test.specDirPath;
    } else {
      // Generate using the standard
      return `test/${this.getTestType()}`;
    }
  }

  public isCoverageEnabled(): boolean {
    const enabled = _.get(this.properties, 'configuration.coverage.enabled');
    return enabled === true || _.isNil(enabled);
  }

  /**
   * Run the initialization before launching test
   */
  protected async runPreTest(): Promise<void> {
    this.logger.debug(`Create the jasmine runner`);
    this.properties.jasmineRunner = new SimpleJasmineRunner();

    this.logger.debug(`Initialize the jasmine runner`);
    await this.properties.jasmineRunner.initialize({
      projectDirectoryPath: this.properties.configuration.project.rootDirectoryPath,
      reportDirectoryPath: this.getRootReportTestPath(),
      specFileFilters: [],
      specDirPath: this.getTestSpecDirPath(),
      features: {
        reports: {
          // TODO Change reporter
          enableHtml: true,
          enableJunitXml: true
        }
      }
    });

    this.logger.debug(`Call the preLaunch steps`);
    const preLaunchContext = this.createStepBaseContext();
    await this.executeSteps({
      groupName: 'preLaunch',
      context: preLaunchContext
    });
  }

  protected async runTest(): Promise<void> {
    // TODO Coverage
    if (this.isCoverageEnabled()) {
      this.logger.debug(`Create coverage instance`);
      this.properties.coverageRunner = new SimpleCoverageRunner();

      this.logger.debug(`Initialize coverage`);
      await this.properties.coverageRunner.initialize({
        reportDirectoryPath: `${this.getRootReportTestPath()}/coverage`,
        projectDirectoryPath: this.getProjectRootDirPath()
      });

      this.logger.debug(`Start coverage`);
      this.properties.coverageRunner.start();
    }

    this.logger.debug(`Call the preTestLaunch steps`);
    const preTestLaunchContext = this.createStepBaseContext();
    await this.executeSteps({
      groupName: 'preTestLaunch',
      context: preTestLaunchContext
    });

    this.logger.debug(`Execute test`);
    const testResponse = await this.properties.jasmineRunner.run({
      executionContext: this.properties.executionContext
    });

    this.logger.debug(`Test finished with success=${testResponse.success}`);
  }

  /**
   * Post test execution
   */
  protected async runPostTest(): Promise<void> {
    this.removeActiveState('running');
    this.addActiveState('postExecution');

    const postExecutionContext = this.createStepBaseContext() as testLauncher.PostExecutionStepHandlerContext;
    postExecutionContext.testSucceed = this.properties.jasmineRunner.lastExecutionSucceed;

    // Set the exit code
    this.logger.debug(`Jasmine return success=${this.properties.jasmineRunner.lastExecutionSucceed}`);
    this.properties.processExitCode = this.properties.jasmineRunner.lastExecutionSucceed ? 0 : 1;

    try {
      this.logger.debug(`Execute postExecution steps`);
      await this.executeSteps({
        groupName: 'postExecution',
        context: postExecutionContext,
        stopOnError: false
      });

      if (this.isCoverageEnabled()) {
        this.logger.debug(`Clean coverage`);
        await this.properties.coverageRunner.clean();
      }
    } catch (error) {
      this.logger.error(`Something went wrong: ${error.message}\n${error.stack}`);
      this.properties.processExitCode = 1;
    }

    this.removeActiveState('postExecution');
  }

  protected createStepBaseContext(): testLauncher.BaseStepHandlerContext {
    return {
      launcher: this,
      logger: this.logger,
      executionContext: this.properties.executionContext
    };
  }

  protected async executeSteps(options: simpleTestLauncher.ExecuteStepOptions): Promise<void> {
    if (!_.isArray(this.properties.steps[options.groupName])) {
      throw new Error(`Cannot find the steps for '${options.groupName}'`);
    }

    // Create a new step list which contains internal steps and external
    const internalSteps = [];
    const prefixName = '_autoStep' + options.groupName.substr(0, 1).toUpperCase() + options.groupName.substr(1);
    const functionNames = _.sortBy(_.filter(classUtils.getInstanceMethodNames(this), test => {
      return _.startsWith(test, prefixName);
    }));

    functionNames.forEach(functionName => {
      internalSteps.push({
        handler: this[functionName].bind(this)
      });
    });

    const steps = _.concat([], internalSteps, this.properties.steps[options.groupName]) as testLauncher.Step[];

    if (steps.length === 0) {
      return;
    }

    let ignoreNextStep = false;
    const errors = [];

    for (const step of steps) {
      if (ignoreNextStep) {
        continue;
      }

      try {
        const result = await step.handler(options.context);
        if (_.isObjectLike(result)) {
          ignoreNextStep = result.stopWorkflow === true;
        }
      } catch (error) {
        if (!options.stopOnError) {
          errors.push(error);
        } else {
          throw error;
        }
      }
    }

    if (errors.length > 0) {
      throw errors[0];
    }
  }

  protected addActiveState(stateName) {
    this.properties.activatedStates.push(stateName);
  }

  protected removeActiveState(stateName) {
    _.remove(this.properties.activatedStates, item => item === stateName);
  }

  /**
   * Save the coverage and may print the summary
   * @param context
   */
  protected async _autoStepPostExecutionSaveCoverage(context: testLauncher.PostExecutionStepHandlerContext): Promise<testLauncher.StepHandlerOnResolve> {
    if (!this.isCoverageEnabled()) {
      this.logger.debug(`Coverage not enabled`);
      return;
    }

    this.logger.debug(`Save the reports`);
    await this.properties.coverageRunner.saveReports();

    if (_.get(this.properties, 'configuration.coverage.summary.enabled') === true) {
      this.logger.debug(`Coverage summary enabled`);
      const summary = await this.properties.coverageRunner.generateSummary({
        template: _.get(this.properties, 'configuration.coverage.summary.template') as string
      });

      process.stdout.write(`\n\n${summary}\n\n`);
    }
  }
}

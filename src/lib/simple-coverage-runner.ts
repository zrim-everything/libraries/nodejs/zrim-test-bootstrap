import {CoverageRunner, coverageRunner} from "./coverage-runner";
import {createLogger, Logger} from "./logger";
import * as NYC from 'nyc';
import {processTemplate} from './utils/process-template';
import * as _ from 'lodash';
import * as fse from 'fs-extra';

export namespace simpleCoverageRunner {

  export interface Properties {
    instance?: object;
    nativeConfiguration?: object;
    configuration?: coverageRunner.configuration.Configuration;
  }

}

export class SimpleCoverageRunner implements CoverageRunner {

  protected logger: Logger;
  protected properties: simpleCoverageRunner.Properties;

  constructor() {

    this.properties = {};
    this.logger = createLogger('coverage');
  }

  public async initialize(options: coverageRunner.configuration.Configuration): Promise<void> {
    this.properties.configuration = options;

    this.properties.nativeConfiguration = {
      sourceMap: true,
      extension: ['.ts'],
      // include: [
      //   "src/**/*.js"
      // ],
      exclude: [
        "**/*.spec.js",
        "*.spec.js",
        "**/*.spec.ts",
        "*.spec.ts"
      ],
      reporter: [
        "lcov",
        // "text-summary",
        'json'
      ],
      // require: ['source-map-support/register'],
      cwd: this.properties.configuration.projectDirectoryPath,
      reportDir: this.properties.configuration.reportDirectoryPath,
      produceSourceMap: true,
      hookRequire: true,
      excludeAfterRemap: true,
      cache: true
    };
    this.logger.debug(`Create the nyc instance`);
    this.properties.instance = new NYC(this.properties.nativeConfiguration);

    this.logger.debug(`Remove the wrap exit`);
    // @ts-ignore
    this.properties.instance._wrapExit = () => {
      // Nothing
    };
  }

  public start() {

    this.logger.debug(`Create the temporary directory`);
    // @ts-ignore
    this.properties.instance.reset();

    this.logger.debug(`Wrap`);
    // @ts-ignore
    this.properties.instance.wrap();
  }

  public async saveReports(): Promise<void> {
    this.logger.debug(`Write the coverage file`);
    // @ts-ignore
    this.properties.instance.writeCoverageFile();

    this.logger.debug(`Generate the reports`);
    // @ts-ignore
    this.properties.instance.report();
  }

  public async clean(): Promise<void> {
    this.logger.debug(`Clean up the instance`);
    await fse.remove(this.properties.instance["tempDirectory"]());
  }

  public async generateSummary(options: coverageRunner.GenerateSummaryOptions): Promise<string> {
    this.logger.debug(`Require the lib report`);
    const libReport = require('istanbul-lib-report');

    this.logger.debug(`Get the map report`);

    const map = await this.properties.instance["getCoverageMapFromAllCoverageFiles"]();

    this.logger.debug(`Get the summarize from the map`);
    const reporterContext = libReport.createContext({
      dir: this.properties.instance["reportDirectory"](),
      watermarks: this.properties.instance["config"].watermarks,
      coverageMap: map
    });

    const tree = reporterContext.getTree();

    let summary: string | undefined;

    const visitor = {
      // onDetail(node)
      // onSummary(node)
      // onStart(node, context)
      onStart(node) {
        summary = node.getCoverageSummary().data;
      }
    };
    tree.visit(visitor, reporterContext);

    const knownKeys = ['lines', 'statements', 'functions', 'branches'];

    const doSum = property => {
      let total = 0;
      _.each(knownKeys, key => {
        total += summary[key][property];
      });
      return total;
    };

    const templateVariables = {
      total: {
        percent: Number((doSum('pct') / 4).toFixed(2)),
        pct: doSum('pct') / 4,
        total: doSum('total'),
        covered: doSum('covered'),
        skipped: doSum('skipped')
      }
    };

    _.each(knownKeys, key => {
      templateVariables[key] = _.cloneDeep(summary[key]);
      templateVariables[key].percent = templateVariables[key].pct;
    });

    this.logger.debug(`Process the template`);
    const {data: generatedSummary} = await processTemplate({
      template: options.template,
      variables: templateVariables
    });

    this.logger.debug(`Returns the generated summary`);
    return generatedSummary;
  }
}

import {SimpleCoverageConfigBuilder} from './coverage-config-builder';
import {SimpleProjectConfigBuilder} from './project-config-builder';
import {SimpleReportsConfigBuilder} from './reports-config-builder';
import {SimpleTestConfigBuilder} from './test-config-builder';
import * as _ from 'lodash';
import {testLauncher} from "../test-launcher";


export namespace simpleTestLauncherConfigBuilder {

  export interface Properties {
    configuration: Configuration;
    projectConfigBuilder?: SimpleProjectConfigBuilder;
    testConfigBuilder?: SimpleTestConfigBuilder;
    reportsConfigBuilder?: SimpleReportsConfigBuilder;
    coverageConfigBuilder?: SimpleCoverageConfigBuilder;
  }

  export interface Configuration {
    steps: Steps;
  }

  export interface Steps {
    preLaunch: Step[];
    preTestLaunch: Step[];
    postExecution: Step[];
    cleanUp: Step[];
  }

  export interface Step {
    handler: Function;
  }
}

/**
 * The configuration builder
 * In case you need to add a sub configuration builder, end you property name by ConfigBuilder, and it will
 * automatically added in the clear and build function.
 * To be handled by the build function, the builder need to have the property _subConfigurationGroupName defined
 */
export class SimpleTestLauncherConfigBuilder {

  /**
   * The properties
   */
  protected properties: simpleTestLauncherConfigBuilder.Properties;

  constructor() {
    this.properties = {
      configuration: this._defaultConfiguration()
    };
    this._createSubConfigurationBuilders();
  }

  /**
   * Returns the test configuration
   */
  public testConfiguration(): SimpleTestConfigBuilder {
    return this.properties.testConfigBuilder;
  }

  /**
   * Returns the reports configuration
   */
  public reportsConfiguration(): SimpleReportsConfigBuilder {
    return this.properties.reportsConfigBuilder;
  }

  /**
   * Returns the coverage configuration
   */
  public coverageConfiguration(): SimpleCoverageConfigBuilder {
    return this.properties.coverageConfigBuilder;
  }

  /**
   * Returns the project configuration
   */
  public projectConfiguration(): SimpleProjectConfigBuilder {
    return this.properties.projectConfigBuilder;
  }

  /**
   * Clear the configuration
   */
  public clear(): SimpleTestLauncherConfigBuilder {
    this.properties.configuration = this._defaultConfiguration();
    // Use a special pattern
    _.each(_.filter(_.keysIn(this.properties), key => _.endsWith(key, 'ConfigBuilder')), key => {
      if (_.isObject(this.properties[key]) && _.isFunction(this.properties[key].clear)) {
        this.properties[key].clear();
      }
    });
    return this;
  }

  /**
   * Add a pre launch step function
   * @param value The step function to add
   */
  public withPreLaunchStep(value: Function): SimpleTestLauncherConfigBuilder {
    if (_.isFunction(value)) {
      this.properties.configuration.steps.preLaunch.push({
        handler: value
      });
    }

    return this;
  }

  /**
   * Add a pre test step function
   * @param value The step function to add
   */
  public withPreTestLaunchStep(value: Function): SimpleTestLauncherConfigBuilder {
    if (_.isFunction(value)) {
      this.properties.configuration.steps.preTestLaunch.push({
        handler: value
      });
    }

    return this;
  }

  /**
   * Add a post execution step function
   * @param value The step function to add
   */
  public withPostExecutionStep(value: Function): SimpleTestLauncherConfigBuilder {
    if (_.isFunction(value)) {
      this.properties.configuration.steps.postExecution.push({
        handler: value
      });
    }

    return this;
  }

  /**
   * Add a clean up step function
   * @param value The step function to add
   */
  public withCleanUpStep(value: Function): SimpleTestLauncherConfigBuilder {
    if (_.isFunction(value)) {
      this.properties.configuration.steps.cleanUp.push({
        handler: value
      });
    }

    return this;
  }

  /**
   * Generate the configuration
   */
  public build(): testLauncher.configuration.Configuration {
    const subConfiguration = {};

    // _subConfigurationGroupName
    _.each(_.filter(_.keysIn(this.properties), key => _.endsWith(key, 'ConfigBuilder')), key => {
      if (_.isObject(this.properties[key]) && _.isFunction(this.properties[key].build) && _.isString(this.properties[key]._subConfigurationGroupName)) {
        subConfiguration[this.properties[key]._subConfigurationGroupName] = this.properties[key].build();
      }
    });

    return _.assign({}, this.properties.configuration, subConfiguration) as testLauncher.configuration.Configuration;
  }

  protected _defaultConfiguration(): simpleTestLauncherConfigBuilder.Configuration {
    return {
      steps: {
        preLaunch: [],
        preTestLaunch: [],
        postExecution: [],
        cleanUp: []
      }
    };
  }

  /**
   * Create the configuration builders
   */
  protected _createSubConfigurationBuilders(): void {
    this.properties.coverageConfigBuilder = new SimpleCoverageConfigBuilder(this);
    this.properties.testConfigBuilder = new SimpleTestConfigBuilder(this);
    this.properties.reportsConfigBuilder = new SimpleReportsConfigBuilder(this);
    this.properties.projectConfigBuilder = new SimpleProjectConfigBuilder(this);
  }
}

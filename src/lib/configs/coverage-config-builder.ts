import {SimpleTestLauncherConfigBuilder} from './test-launcher-config-builder';
import * as _ from 'lodash';
import {testLauncher} from "../test-launcher";

export namespace SimpleCoverageConfigBuilder {

  export interface Properties {
    parent: SimpleTestLauncherConfigBuilder | undefined;
    configuration: Configuration;
  }

  export interface Summary {
    enabled?: boolean | undefined;
    template?: string | undefined;
  }

  export interface Configuration {
    enabled?: boolean;
    summary?: Summary;
  }
}

const summaryTemplate: string = '' +
  '=============================== Coverage summary ===============================\n' +
  'Total: {{ total.percent }}% covered\n' +
  'Statements: {{ statements.percent }}% ({{ statements.covered }}/{{ statements.total }})<% if(statements.skipped > 0) { print(", " + statements.skipped + " ignored") } %>\n' +
  'Branches: {{ branches.percent }}% ({{ branches.covered }}/{{ branches.total }})<% if(branches.skipped > 0) { print(", " + branches.skipped + " ignored") } %>\n' +
  'Functions: {{ functions.percent }}% ({{ functions.covered }}/{{ functions.total }})<% if(functions.skipped > 0) { print(", " + functions.skipped + " ignored") } %>\n' +
  'Lines: {{ lines.percent }}% ({{ lines.covered }}/{{ lines.total }})<% if(lines.skipped > 0) { print(", " + lines.skipped + " ignored") } %>\n' +
  '================================================================================\n';

export class SimpleCoverageConfigBuilder {

  protected properties: SimpleCoverageConfigBuilder.Properties;

  constructor(parent?: SimpleTestLauncherConfigBuilder) {
    this.properties = {
      parent,
      configuration: this._defaultConfiguration()
    };
  }

  /**
   * The configuration group name
   */
  public get _subConfigurationGroupName(): string {
    return 'coverage';
  }

  /**
   * Clear the configuration
   */
  public clear(): SimpleCoverageConfigBuilder {
    this.properties.configuration = this._defaultConfiguration();
    return this;
  }

  /**
   * Enable the coverage
   */
  public enable(): SimpleCoverageConfigBuilder {
    this.properties.configuration.enabled = true;
    return this;
  }

  /**
   * Disable the coverage
   */
  public disable(): SimpleCoverageConfigBuilder {
    this.properties.configuration.enabled = false;
    return this;
  }

  /**
   * Set the enable value
   * @param [value] The new value. Set undefined to set put the default value
   */
  public setEnabled(value?: boolean|null): SimpleCoverageConfigBuilder {
    this.properties.configuration.enabled = !!value;
    return this;
  }

  /**
   * Set the summary template.
   *
   * The template is a lodash template.
   * @param [value] The new value. Set undefined to set put the default value
   */
  public summaryTemplate(value?: string): SimpleCoverageConfigBuilder {
    if (_.isNil(value)) {
      this.properties.configuration.summary.template = this._defaultConfiguration().summary.template;
    } else if (_.isString(value)) {
      this.properties.configuration.summary.template = value;
    }

    return this;
  }

  /**
   * Enable or not the summary
   * @param [value] The new value. Set undefined to set put the default value
   */
  public enableSummary(value?: boolean): SimpleCoverageConfigBuilder {
    if (_.isNil(value)) {
      this.properties.configuration.summary.enabled = this._defaultConfiguration().summary.enabled;
    } else if (_.isBoolean(value)) {
      this.properties.configuration.summary.enabled = value;
    } else if (_.isString(value)) {
      this.properties.configuration.summary.enabled = (value as string).toLowerCase() === 'true';
    }

    return this;
  }


  /**
   * Returns the parent builder
   */
  public parentBuilder(): SimpleTestLauncherConfigBuilder {
    return this.properties.parent;
  }

  /**
   * Generate the object configuration
   */
  public build(): testLauncher.configuration.coverage.Configuration {
    const userConfig = {
      summary: {
        template: this.properties.configuration.summary.template
      }
    } as testLauncher.configuration.coverage.Configuration;

    if (!_.isNil(this.properties.configuration.enabled)) {
      userConfig.enabled = this.properties.configuration.enabled;
    }

    if (!_.isNil(this.properties.configuration.summary.enabled)) {
      userConfig.summary.enabled = this.properties.configuration.summary.enabled;
    }

    return _.merge({}, userConfig);
  }


  /**
   * Returns the default configuration
   */
  protected _defaultConfiguration(): SimpleCoverageConfigBuilder.Configuration {
    return {
      enabled: undefined,
      summary: {
        enabled: undefined,
        template: summaryTemplate
      }
    };
  }
}

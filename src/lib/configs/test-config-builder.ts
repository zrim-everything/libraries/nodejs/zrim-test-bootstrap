import {SimpleTestLauncherConfigBuilder} from './test-launcher-config-builder';
import * as _ from 'lodash';
import {testLauncher} from "../test-launcher";

export namespace simpleTestConfigBuilder {

  export interface Properties {
    parent: SimpleTestLauncherConfigBuilder | undefined;
    configuration: Configuration;
  }

  export interface Configuration {
    type: string;
    specDirPath: string | undefined;
    nativeConfiguration: object;
    specFileFilters: Array<Function|string|RegExp>;
    features: Features;
  }

  export interface Features {
    enableJunitXml: boolean;
    enableHtml: boolean;
  }
}

export class SimpleTestConfigBuilder {

  protected properties: simpleTestConfigBuilder.Properties;

  constructor(parent?: SimpleTestLauncherConfigBuilder) {
    this.properties = {
      parent,
      configuration: this._defaultConfiguration()
    };
  }

  /**
   * The configuration group name
   */
  public get _subConfigurationGroupName(): string {
    return 'test';
  }

  /**
   * Clear the configuration
   */
  public clear(): SimpleTestConfigBuilder {
    this.properties.configuration = this._defaultConfiguration();
    return this;
  }

  /**
   * Set the spec directory
   * @param [value] The new value. Set undefined to set put the default value
   */
  public specDirPath(value?: string|null): SimpleTestConfigBuilder {
    if (_.isNil(value)) {
      this.properties.configuration.specDirPath = this._defaultConfiguration().specDirPath;
    } else if (_.isString(value)) {
      this.properties.configuration.specDirPath = value;
    }

    return this;
  }

  /**
   * Set the test type
   * @param [value] The new value. Set undefined to set put the default value
   */
  public type(value?: string|null): SimpleTestConfigBuilder {
    if (_.isNil(value)) {
      this.properties.configuration.type = this._defaultConfiguration().type;
    } else if (_.isString(value)) {
      this.properties.configuration.type = value;
    }

    return this;
  }

  /**
   * Set the type to unit test
   * @see type
   */
  public unitTest(): SimpleTestConfigBuilder {
    return this.type('unit');
  }

  /**
   * Set the type to integration test
   * @see type
   */
  public integrationTest(): SimpleTestConfigBuilder {
    return this.type('integration');
  }

  /**
   * Set the type to system test
   * @see type
   */
  public systemTest(): SimpleTestConfigBuilder {
    return this.type('system');
  }

  /**
   * Set the native configuration
   * @param [value] The new value. Set undefined to set put the default value
   */
  public nativeConfiguration(value?: object|null): SimpleTestConfigBuilder {
    if (_.isNil(value)) {
      this.properties.configuration.nativeConfiguration = this._defaultConfiguration().nativeConfiguration;
    } else if (_.isObject(value)) {
      this.properties.configuration.nativeConfiguration = value;
    }

    return this;
  }

  /**
   * Add a new spec filter
   * @param [value] The filter to add
   */
  public withSpecFileFilter(value?: Function|string|RegExp): SimpleTestConfigBuilder {
    if (_.isFunction(value) || _.isRegExp(value) || _.isString(value)) {
      this.properties.configuration.specFileFilters.push(value);
    }

    return this;
  }

  /**
   * Returns the parent builder
   */
  public parentBuilder(): SimpleTestLauncherConfigBuilder {
    return this.properties.parent;
  }

  /**
   * Generate the object configuration
   */
  public build(): testLauncher.configuration.test.Configuration {
    const userConfig = {
      type: this.properties.configuration.type,
      specFileFilters: this.properties.configuration.specFileFilters,
      specDirPath: this.properties.configuration.specDirPath,
      jasmine: {
        features: {
          enableJunitXml: this.properties.configuration.features.enableJunitXml,
          enableHtml: this.properties.configuration.features.enableHtml
        },
        nativeConfig: this.properties.configuration.nativeConfiguration
      }
    } as testLauncher.configuration.test.Configuration;

    return userConfig;
  }


  /**
   * Returns the default configuration
   */
  protected _defaultConfiguration(): simpleTestConfigBuilder.Configuration {
    return {
      type: 'unknown',
      specDirPath: undefined,
      nativeConfiguration: {},
      specFileFilters: [],
      features: {
        enableJunitXml: true,
        enableHtml: true
      }
    };
  }
}

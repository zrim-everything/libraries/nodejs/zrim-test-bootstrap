# Configuration helpers

```javascript
require('zrim-test-bootstrap').configs
```

## Introduction

Contains helper to generate the configuration

## Builder

Using a configuration builder may help to make the configuration more understandable.

The root builder for the launcher is [SimpleTestLauncherConfigBuilder](test-launcher-config-builder.md).


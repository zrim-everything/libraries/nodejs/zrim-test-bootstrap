# Test Launcher Configuration Builder

```javascript
require('zrim-test-bootstrap').configs.SimpleTestLauncherConfigBuilder
```

```typescript
import {configs} from 'zrim-test-bootstrap';
import SimpleTestLauncherConfigBuilder = configs.SimpleTestLauncherConfigBuilder;
```

## Introduction

Simple configuration builder to generate the launcher configuration.

## Usage

```javascript
const configBuilder = new SimpleTestLauncherConfigBuilder();

const config = configBuilder
  // Configure
  .build();
```

## Methods

### testConfiguration

```
testConfiguration() : SimpleTestConfigBuilder
```

Returns the [test configuration builder](test-config-builder.md)

### reportsConfiguration

```
reportsConfiguration() : SimpleReportsConfigBuilder
```

Returns the [reports configuration builder](reports-config-builder.md)

### coverageConfiguration

```
coverageConfiguration() : SimpleCoverageConfigBuilder
```

Returns the [coverage configuration builder](coverage-config-builder.md)

### projectConfiguration

```
projectConfiguration() : SimpleCoverageConfigBuilder
```

Returns the [project configuration builder](project-config-builder.md)

### clear

```
clear() : SimpleTestLauncherConfigBuilder
```

Clear the whole configuration

### withPreLaunchStep

```
withPreLaunchStep(step : Function) : SimpleTestLauncherConfigBuilder
```

Add a step to be executed before launching the system.
This is executed before coverage a test.

### withPreTestLaunchStep

```
withPreTestLaunchStep(step : Function) : SimpleTestLauncherConfigBuilder
```

Add a step to be executed before launching the test suite.
This is executed after the coverage started and before launching the test.

### withPostExecutionStep

```
withPostExecutionStep(step : Function) : SimpleTestLauncherConfigBuilder
```

Add a step to be executed after the test suite finished.

### withCleanUpStep

```
withCleanUpStep(step : Function) : SimpleTestLauncherConfigBuilder
```

Execute after everything finished event if the test never started due to error.

### build

```
build() : Object
```

Build the configuration.



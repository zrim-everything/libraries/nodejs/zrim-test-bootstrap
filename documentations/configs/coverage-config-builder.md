# Coverage Configuration Builder

```javascript
require('zrim-test-bootstrap').configs.SimpleCoverageConfigBuilder
```

```typescript
import {configs} from 'zrim-test-bootstrap';
import SimpleCoverageConfigBuilder = configs.SimpleCoverageConfigBuilder;
```

## Introduction

Help to generate the configuration for the coverage

## Methods

### construct

The constructor may receive a options:
- parent [SimpleTestLauncherConfigBuilder](test-launcher-config-builder.md) : 
The parent configuration builder

### clear

```
clear() : SimpleCoverageConfigBuilder
```

Clear the configuration

### enable

```
enable() : SimpleCoverageConfigBuilder
```

Enable the coverage (alias for `setEnabled(true)`)

### disable

```
disable() : SimpleCoverageConfigBuilder
```

Disable the coverage (alias for `setEnabled(false)`)

### setEnabled

```
setEnabled(value : boolean): SimpleCoverageConfigBuilder
```

Enable or not the coverage. Passing undefined will set the default value

### reportDirectory

```
reportDirectory(path: string): SimpleCoverageConfigBuilder
```

Set the report directory path


### summaryTemplate

```
nativeConfiguration(value: string): SimpleCoverageConfigBuilder
```

Set the summary template.

### enableSummary

```
enableSummary(value: boolean): SimpleCoverageConfigBuilder
```

Set true to enable the summary

### parentBuilder

```
parentBuilder() : SimpleTestLauncherConfigBuilder
```

Returns the [parent builder](test-launcher-config-builder.md)

The value returned is the one given to the constructor.

### build

```
build(): Object
```

Generate the configuration
